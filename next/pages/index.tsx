import { default as React } from 'react';

const Index = (props: any) => {
  return (
    <div>
      <h1>Next, GraphQL TypeScript, Prisma, PostgreSQL</h1>
      <p>... It's all running in docker ;)</p>
    </div>
  );
};

export default Index;