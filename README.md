# Kobold

## To get started...

Step 1:
```
npm i
```

Step 2:
```
docker-compose up -d
```

Step 3:
```
???
```

Step 4:
```
Profit
```

## Tips...

If the typescript client or next application stop, or need debugger, use can use...

```
docker exec -it [container] /bin/sh;
```

to open an interactive terminal.